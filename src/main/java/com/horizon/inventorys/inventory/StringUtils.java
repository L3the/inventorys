package com.horizon.inventorys.inventory;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {

    /**
     * Splits the String in equal seperate parts.
     *
     * @param text - The String to splot
     * @param n    - The Size of the Substring
     * @return - The List of the Substrings
     */
    public static List<String> getSubStrings(String text, int n) {
        ArrayList<String> substrings = new ArrayList<String>();
        int lastsubstringpos = 0;
        for (int i = 0; i < text.length(); i++) {
            if (i % n == 0) {
                substrings.add(text.substring(lastsubstringpos, i));
                lastsubstringpos = i;
            }
        }
        substrings.add(text.substring(lastsubstringpos, text.length()));
        return substrings;
    }
}
