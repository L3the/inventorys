# InventoryS - Easy to use InventoryAPI #
(c) 2015 by TheSilentHorizon
InventoryS is an easy-to-use System to create simple Menus for Minecraft Gamemodes. It has an special Item-Event Bind System to trigger events.
## How to use? ##

###Creating an Inventory###

Creating an Inventory is very easy. Just follow the given example. You need to create an Instance of Inventory (Note: Not the Inventory of Bukkit, these classes have the same name!).
Create now as many SlotItems as you need, and add them by addSlotItem to the Inventory. You also can dynamically change the Contents, which allows you to create many different styles!



```
#!java
//Creates an Simple Inventory
Inventory inv = new Inventory("Testinv",3);
//Creates a SlotItem with the given information.
SlotItem item = new SlotItem("A Item","This is a description.",0,Material.NETHER_STAR);
//Adds the item on position 11 to the Inventory.
inv.addSlotItem(11, item);
//Registers the Inventory to the Listener.
InventoryS.registerInventory(yourplugininstance, inv);

```

###Mount some action to your SlotItem###

To tell the Plugin, what should get executed, when you click on the Item, you need to bind an InventoryRunnable to it. You can provide the functions you want to call, inside of InventoryRunnables abstract function. An example bind would look like this:


```
#!java

//Create a new Instance of an SlotItem.
SlotItem item = new SlotItem("A Item","Pls click on the Netherstar!.",0,Material.NETHER_STAR);
//Bind a new InventoryRunnable to it.
item.setOnClick(new InventoryRunnable(){
	@Override
	public void runOnClick(InventoryClickEvent e) {
		// Here you can put any code you want to.
		e.getWhoClicked().sendMessage("You clicked on the netherstar!");
	}
});

```

###Create a HotbarItem###

A HotbarItem can be created like an SlotItem. But, it has to be registered too by using InventoryS.registerHotbarItem(...);


```
#!java

//Create the HotbarItem.
HotbarItem hbi = new HotbarItem("A MenuItem.","It calls a Menu.",0,Material.FIREWORK);
//Register it.
InventoryS.registerHotbarItem(yourplugininstance, hbi);
```

###Mount action to your HotbarItem###

The HotbarItem works like the SlotItem, but it will be only used on the PlayerInteractEvent. So you can define in the head, which Action you want to accept, for triggering your HotbarItem.


```
#!java

//Create the HotbarItem.
HotbarItem hbi = new HotbarItem("A MenuItem.","It calls a Menu.",0,Material.FIREWORK);
//You can set Actions as params, to define, on which Action, this HotbarItem should get triggered.
//If you insert it like in this example, the onHotbarItemUsed is always triggered, when you click in the air.
hbi.setOnClick(new HotbarRunnable(Action.RIGHT_CLICK_AIR,Action.LEFT_CLICK_AIR){
	
	@Override
	public void onHotbarItemUsed(PlayerInteractEvent e) {
		//Use whatever code you want. We just want to open an inventory here.
		InventoryS.openInventory(e.getPlayer(), "Hauptmemü");
	}
});

```

###OptionWindow###

An OptionWIndow is a default inventory, which lets the player choose between two Options (yes/no). 

![Unbenannt-2.png](https://bitbucket.org/repo/RyxqaK/images/2260083728-Unbenannt-2.png)


```
#!java

//Simple call this Method
OptionWindow w = InventoryS.showYesNoDialog(instance,player, "Satz drucken?", "Aw yiss", "Nope.");

```
